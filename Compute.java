/**
*
* abstract class Compute
*
* a parent class used to define different computations
*
*/
abstract class Compute{
    public abstract int compute(int n); // implemented by a child class to carry out computation
    // example of open recursion
}
