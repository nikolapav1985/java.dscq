/**
* public class Main
*
* a class used to run this example (contains main method)
*
* ----- example run -----
*
* java Main 5
*
* ----- example output -----
*
* product 120
* sum 15
*
* ----- compile -----
*
* javac -g Main.java
*
*/
public class Main{
    public static void main(String []args){
        int n; // a starting point for computation
        int pdone; // a starting point for computation
        int sdone; // a starting point for computation
        Compute product; // open recursion, first part
        Compute sum; // open recursion, first part
        if(args.length != 1){ // need one command line parameter
            System.exit(1);
        }
        n=Integer.parseInt(args[0]);
        product=new Product();
        sum=new Sum();
        pdone=product.compute(n); // open recursion, second part, use all objects to do computation
            // actual computation done depends on the child class
        sdone=sum.compute(n);
        System.out.println("product "+pdone);
        System.out.println("sum "+sdone);
    }
}
