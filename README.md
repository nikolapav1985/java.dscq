Discussion 6
------------

- Compute.java (file, parent class for open recursion example)
- Product.java (file, child class for open recursion example)
- Sum.java (file, child class for open recursion example)
- Main.java (file, run example, check comments for details)

Test environment
----------------

- javac version 14.0.2
- java version 14.0.2
- os lubuntu 16.04 lts kernel version 4.13.0
