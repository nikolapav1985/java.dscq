/**
*
* public class Product
*
* a class used to compute a product of natural numbers recursively aka factorial
*
*/
public class Product extends Compute{
    public int compute(int n){ // compute a product of natural numbers recursively
        // aka factorial
        if(n==0) return 1;
        else return n*this.compute(n-1); // open recursion example, 
            // compute can be called by any object
            // object must be instance of a child class 
    }
}
